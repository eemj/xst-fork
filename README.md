# xst-fork

`xst` is an [xst](https://github.com/gnotclub/xst) fork with some additional patches applied

## Suckless patches

- [alpha](https://st.suckless.org/patches/alpha/)
- [boxdraw](https://st.suckless.org/patches/boxdraw/)
- [clipboard](https://st.suckless.org/patches/clipboard/)
- [disable bold/italic fonts](https://st.suckless.org/patches/disable_bold_italic_fonts/)
- [externalpipe](https://st.suckless.org/patches/externalpipe/)
- [scrollback](https://st.suckless.org/patches/scrollback/)
- [spoiler](https://st.suckless.org/patches/spoiler/)
- [vertcenter](https://st.suckless.org/patches/vertcenter/)
- [**ligatures**](https://st.suckless.org/patches/ligatures)
